#pragma once
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <unordered_map>

/*
 * Container for possible pieces positions
 *
 * RL - rigth-left
 * LR - left-right
 * TB - top-bottom
 * BT - bottom-top
 */
enum Position {
  RL,
  LR,
  TB,
  BT
};

const std::initializer_list<Position> all_pos = { RL, LR, TB, BT};

/*
 * Class for calculating sum of squared distance(SSD)
 */
class SSD {
public:
  static double calc(const cv::Mat& mat1, const cv::Mat& mat2, const Position & pos);
private:
  static double calc_ssd(const cv::Mat& first_vec, const cv::Mat& second_vec);
};