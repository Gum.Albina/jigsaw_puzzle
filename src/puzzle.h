#pragma once
#include <iostream>
#include <vector>
#include "SSD.h"

/*
* Container for piece's side
*/
enum SIDE {
  UP,
  DOWN,
  LEFT,
  RIGHT
};

/*
 * Class for puzzle fuctions
 */
class Puzzle {
public:
  static std::vector<cv::Mat> get_pieces(const cv::Mat& image, const int piece_size);
  static const cv::Mat get_side(const cv::Mat& piece, SIDE side, bool near_side=false);
};

