#include "SSD.h"
#include "puzzle.h"


/*
 * Calculate ssd between 2 vectors
 *
 * @param first_vec - first vector (border of piece) 
 * @param second_vec - second vector (border of piece) 
 * @return value of ssd
 */
double SSD::calc_ssd(const cv::Mat& first_vec, const cv::Mat& second_vec) {
  double ssd = 0;
  cv::Mat_<cv::Vec3b> diff = cv::Mat_<cv::Vec3b>();
  absdiff(first_vec, second_vec, diff);
  for (const auto& pixel : diff)
  {
    ssd += pixel[0] * pixel[0] + pixel[1] * pixel[1] + pixel[2] * pixel[2];
  }

  return ssd;
}


/*
* Calculate ssd for 2 pieces in given position
*
* Get the necessary border of given pieces and pass it to SSD::calc_ssd() function
*
* @param mat1 - first piece of puzzle
* @param mat2 - second piece of puzzle
* @param pos - position between given pieces
* @return value of ssd
*/
double SSD::calc(const cv::Mat & mat1, const cv::Mat & mat2, const Position & pos)
{
  double ssd = 0;
  cv::Mat first_side;
  cv::Mat second_side;

  switch (pos)
  {
  case RL:
  {
    first_side = Puzzle::get_side(mat1, RIGHT);
    second_side = Puzzle::get_side(mat2, LEFT);
    
    break;
  }
  case LR:
  {
    first_side = Puzzle::get_side(mat1, LEFT);
    second_side = Puzzle::get_side(mat2, RIGHT);
    break;
  }
  case TB:
  {
    first_side = Puzzle::get_side(mat1, DOWN);
    second_side = Puzzle::get_side(mat2, UP);
    break;
  }
  case BT:
  {
    first_side = Puzzle::get_side(mat1, UP);
    second_side = Puzzle::get_side(mat2, DOWN);

    break;
  }
  default:
    break;
  }
  
  ssd = calc_ssd(first_side, second_side);
  return ssd;
}