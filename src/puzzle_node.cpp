#include "puzzle_node.h"

/*
 * Constructor for PuzzleNode class
 *
 * @param index - index of current puzzle piece
 */
PuzzleNode::PuzzleNode(int index)
{
  m_index = index; 
  m_up = nullptr;
  m_left = nullptr;
  m_right = nullptr;
  m_down = nullptr;
}

/*
 * Add neighbour for current piece by given side
 *
 * @param neighbour - new neighbour
 * @param side - pieces merge position
*/
void PuzzleNode::set_neighbour(PuzzleNode* neighbour, SIDE side)
{
  switch (side)
  {
  case RIGHT:
    m_right = neighbour;
    break;
  case LEFT:
    m_left = neighbour;
    break;
  case UP:
    m_up = neighbour;
    break;
  case DOWN:
    m_down = neighbour;
    break;
  default:
    break;
  }
}

/*
 * Get up neighbour
 *
 * @return ptr to up neighbour puzzle node
 */
PuzzleNode * PuzzleNode::get_up_neighbour()
{
  return m_up;
}

/*
 * Get left neighbour
 *
 * @return ptr to left neighbour puzzle node
 */
PuzzleNode * PuzzleNode::get_left_neighbour()
{
  return m_left;
}

/*
* Get down neighbour
*
* @return ptr to down neighbour puzzle node
*/
PuzzleNode * PuzzleNode::get_down_neighbour()
{
  return m_down;
}

/*
* Get right neighbour
*
* @return ptr to right neighbour puzzle node
*/
PuzzleNode * PuzzleNode::get_right_neighbour()
{
  return m_right;
}

/*
* Overload == operator for PuzzleNode class
*
* Two PuzzleNode's are equal if equal their indices
*
* @return true if instances are equal, false otherwise
*/
bool PuzzleNode::operator==(const PuzzleNode & node) const
{
  return (m_index == node.m_index);
}
