#include "MGC.h"

/*
 * lay out the pixels of given matrix by channels
 *
 * @param mat - matrix to lay out
 * @return matrix with separated channels value
 */
cv::Mat_<double> MGC::get_mat_per_channels(const cv::Mat_<cv::Vec3d>& mat)
{
  cv::Mat_<double> tmp[3];
  cv::Mat_<double> res = cv::Mat_<double>(mat.channels(), mat.cols);

  split(mat, tmp);
  cv::Mat matArray[] = { tmp[0], tmp[1], tmp[2] };
  vconcat(matArray, 3, res);
  
  return (cv::Mat_<double>)res.t();
}

/*
 * Calculate distance in direction from the first side to the second
 *
 * Calculate pixels diff between the first and second sides - diff
 * Calculate pixels diff between first side and neighbour - local_diff
 * Calculate covariance matrix and mean for first piece edge - side_mean, cov
 * Calculate distance as sum((diff - side_mean) * inv(cov) * (diff - side_mean))
 *
 * @param first_side - side of first piece
 * @param second_side - side of second piece
 * @param neighbour_for_side - neighbour side for first side
 * @return value of the distance
 */
double MGC::get_dist(const cv::Mat &first_side, const cv::Mat& second_side, const cv::Mat& neighbour_for_side)
{
  double dist = 0;
  cv::Mat_<cv::Vec3d> diff = cv::Mat_<cv::Vec3d>();
  cv::Mat_<cv::Vec3d> local_diff = cv::Mat_<cv::Vec3d>();
  cv::Mat_<double> local_diff_per_channel;
  cv::Mat_<double> cov , side_mean;
  
  absdiff(first_side, second_side, diff);
  absdiff(first_side, neighbour_for_side, local_diff);
  local_diff_per_channel = get_mat_per_channels(local_diff);
  calcCovarMatrix(local_diff_per_channel, cov, side_mean, CV_COVAR_NORMAL | CV_COVAR_ROWS | CV_COVAR_SCALE);

  cv::Mat_<double> inv_cov = (cv::Mat_<double>)cov.inv();
  for (const auto& pixel : diff)
  {
    cv::Mat_<double> vec = (cv::Mat_<double>)(pixel - (cv::Vec3d)side_mean);
    dist += ((cv::Mat_<double>)(vec.t() * cov.inv() * vec)).at<double>(0, 0);
  }

  return dist;
}


/*
* Calculate distance(similarity) for 2 pieces in given position
*
* Get the necessary border of given pieces and pass it to SSD::get_dist() function
*
* @param mat1 - first piece of puzzle
* @param mat2 - second piece of puzzle
* @param pos - position between given pieces
* @return value of distance
*/
double MGC::calc(const cv::Mat & mat1, const cv::Mat & mat2, const Position & pos)
{
  cv::Mat first_side, neighbour_for_first_side;
  cv::Mat second_side, neighbour_for_second_side;
  Position paired_pos;

  switch (pos)
  {
  case RL:
  {
    first_side = Puzzle::get_side(mat1, RIGHT);
    neighbour_for_first_side = Puzzle::get_side(mat1, RIGHT, true);
    second_side = Puzzle::get_side(mat2, LEFT);
    neighbour_for_second_side = Puzzle::get_side(mat2, LEFT, true);
    paired_pos = LR;

    break;
  }
  case LR:
  {
    first_side = Puzzle::get_side(mat1, LEFT);
    neighbour_for_first_side = Puzzle::get_side(mat1, LEFT, true);
    second_side = Puzzle::get_side(mat2, RIGHT);
    neighbour_for_second_side = Puzzle::get_side(mat2, RIGHT, true);
    paired_pos = RL;
    break;
  }
  case TB:
  {
    first_side = Puzzle::get_side(mat1, DOWN);
    neighbour_for_first_side = Puzzle::get_side(mat1, DOWN, true);
    second_side = Puzzle::get_side(mat2, UP);
    neighbour_for_second_side = Puzzle::get_side(mat2, UP, true);
    paired_pos = BT;
    break;
  }
  case BT:
  {
    first_side = Puzzle::get_side(mat1, UP);
    neighbour_for_first_side = Puzzle::get_side(mat1, UP, true);
    second_side = Puzzle::get_side(mat2, DOWN);
    neighbour_for_second_side = Puzzle::get_side(mat2, DOWN, true);
    paired_pos = TB;
    break;
  }
  default:
    break;
  }

  return get_dist(first_side, second_side, neighbour_for_first_side) +
    get_dist(second_side, first_side, neighbour_for_second_side);
}





