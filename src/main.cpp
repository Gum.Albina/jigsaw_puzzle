#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include "puzzle_solver.h"
#include "SSD.h"


using namespace std;
using namespace cv;
namespace po = boost::program_options;

/*
 * Arguments parser
 * 
 * @param argc - length of argv param
 * @param atgv - string(array) of given arguments
 * @return map of pairs (argument, value)
 */
po::variables_map parse_args(int argc, char** argv)
{
  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("input,i", po::value<std::string>(), "write -i [path to input file]")
    ("block,b", po::value<int>(), "write -b [block size]. There are 3 possible sizes: 16, 32 and 64 pixels")
    ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << "\n";
  }

  return vm;
}



int main(int argc, char** argv)
{
  const auto& vm = parse_args(argc, argv);

  int piece_size = vm["block"].as<int>();

  Mat image = imread(vm["input"].as<string>(), IMREAD_COLOR);

  PuzzleSolver solver;
  solver.solve(image, piece_size);

  system("pause");
  return 0;
}

