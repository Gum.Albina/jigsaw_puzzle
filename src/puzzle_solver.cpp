#include <unordered_set>
#include <algorithm>
#include "puzzle_solver.h"
#include "MGC.h"

/*
 * Overload operator == for connection struct
 * 
 * Connections are equal if equal both pieces indices, position and weight
 *
 * @return true if instances are equal, false otherwise
 */
bool Connection::operator==(const Connection& con) const
{
  return (con.m_first_index == m_first_index && con.m_second_index == m_second_index &&
    con.m_pos == m_pos && con.m_weight == m_weight);
}

/*
* Overload operator < for connection struct
*
* Connections compare by weights
*
* @return true if first connection is less than second, false otherwise
*/
bool Connection::operator<(const Connection& con) const
{
  return con.m_weight < m_weight;
}

/*
 * Constructor for PuzzleSolver class
 */
PuzzleSolver::PuzzleSolver() {
  m_mst_max_size = 0;
  cons = std::vector<Connection>();
}

/*
* Destructor for PuzzleSolver class
*/
PuzzleSolver::~PuzzleSolver() {
  cons.clear();
}

/*
 * Merge 2 nodes(pieces) in 1 graph
 *
 * @param first - first puzzle piece to merge
 * @param second - second puzzle piece to merge
 * @param pos - pieces merge position
 */
void PuzzleSolver::merge_nodes(PuzzleNode* first, PuzzleNode* second, Position pos)
{
  switch (pos)
  {
  case RL:
    first->set_neighbour(second, LEFT);
    second->set_neighbour(first, RIGHT);
    break;
  case LR:
    first->set_neighbour(second, RIGHT);
    second->set_neighbour(first, LEFT);
    break;
  case TB:
    first->set_neighbour(second, DOWN);
    second->set_neighbour(first, UP);
    break;
  case BT:
    first->set_neighbour(second, UP);
    second->set_neighbour(first, DOWN);
    break;
  default:
    break;
  }
}


/*
 * Get neighbours of given node and check if there is collapse
 *
 * Sequentially call this function to all cur_node neighbours
 * Add neighbours' positions to positions class
 * And check collapses
 * (Collapse is overlay of 2 pieces in 1 position)
 *
 * @param cur_node - current node(puzzle piece) 
 * @param positions - current nodes positions in graph
 * @param last_node - index of node where we came from
 * @return true if there is no collapse, false otherwise
 */
bool PuzzleSolver::get_neighbours_positions(PuzzleNode* cur_node, Node_coords* positions, int last_node)
{
  int x = 0;
  int y = 0;
  if (positions->empty())
    positions->insert(MyPoint(x, y), cur_node);
  else
  {
    MyPoint cur_pos = positions->get_point(cur_node);
    x = cur_pos.get_x();
    y = cur_pos.get_y();
  }
  if (cur_node->get_up_neighbour() != nullptr && cur_node->get_up_neighbour()->get_index() != last_node)
  {
    if (positions->contains(MyPoint(x, y + 1)))
      return false;
    (*positions).insert(MyPoint(x, y + 1), cur_node->get_up_neighbour());
    if (!get_neighbours_positions(cur_node->get_up_neighbour(), positions, cur_node->get_index()))
      return false;
  }

  if (cur_node->get_down_neighbour() != nullptr && cur_node->get_down_neighbour()->get_index() != last_node)
  {
    if (positions->contains(MyPoint(x, y - 1)))
      return false;
    (*positions).insert(MyPoint(x, y - 1), cur_node->get_down_neighbour());
    if (!get_neighbours_positions(cur_node->get_down_neighbour(), positions, cur_node->get_index()))
      return false;
  }

  if (cur_node->get_left_neighbour() != nullptr && cur_node->get_left_neighbour()->get_index() != last_node)
  {
    if (positions->contains(MyPoint(x - 1, y)))
      return false;
    (*positions).insert(MyPoint(x - 1, y), cur_node->get_left_neighbour());
    if (!get_neighbours_positions(cur_node->get_left_neighbour(), positions, cur_node->get_index()))
      return false;
  }

  if (cur_node->get_right_neighbour() != nullptr && cur_node->get_right_neighbour()->get_index() != last_node)
  {
    if (positions->contains(MyPoint(x + 1, y)))
      return false;
    (*positions).insert(MyPoint(x + 1, y), cur_node->get_right_neighbour());
    if (!get_neighbours_positions(cur_node->get_right_neighbour(), positions, cur_node->get_index()))
      return false;
  }

  if (positions->node_from_point.size() > get_mst_max_size())
    set_mst_max_size((int)positions->node_from_point.size());

  return true;
}


/*
 * Check if 2 puzzle pieces can be merged by given position
 *
 * Firtsly, try to add the second node near the first if the first haven't neighbour in given pos
 * Then check if this merging doesn't collapse to all other first's neighbours
 *
 * @param first - first puzzle piece
 * @param second - second puzzle piece
 * @perem pos - pieces merge position
 * @return True if pieces can be merged, false otherwise
 */
bool PuzzleSolver::check_compatibility(PuzzleNode* first, PuzzleNode* second, Position pos)
{
  Node_coords positions = Node_coords();
  get_neighbours_positions(first, &positions, first->get_index());

  switch (pos)
  {
  case RL:
    if (positions.contains(MyPoint(-1, 0)))
      return false;
    positions.insert(MyPoint(-1, 0), second);
    break;
  case LR:
    if (positions.contains(MyPoint(1, 0)))
      return false;
    positions.insert(MyPoint(1, 0), second);
    break;
  case TB:
    if (positions.contains(MyPoint(0, -1)))
      return false;
    positions.insert(MyPoint(0, -1), second);
    break;
  case BT:
    if (positions.contains(MyPoint(0, 1)))
      return false;
    positions.insert(MyPoint(0, 1), second);
    break;
  default:
    break;
  }

  return get_neighbours_positions(second, &positions, first->get_index());
}

/*
 * Pick all pieces in one image by their connections
 *
 * Create template for future result image
 * Find piece for left up corner
 * Then build all image by moving left to right, top to bottom
 *
 * @param puzzle_size - puzzle size in pixels
 * @param pieces - all puzzle pieces
 * @param nodes - result graph of pieces
 * @return result of puzzle assembly
 */
cv::Mat PuzzleSolver::get_result_image(int puzzle_size, const std::vector<cv::Mat>& pieces, std::vector<PuzzleNode>* nodes)
{
  int piece_size = (pieces)[0].cols;
  cv::Mat image = cv::Mat::zeros(puzzle_size + 2 * piece_size, puzzle_size + 2 * piece_size, (pieces)[0].type());
  Node_coords positions = Node_coords();
  get_neighbours_positions(&(*nodes)[0], &positions, 0);

  int min_x = (int)pieces.size();
  int max_y = -((int)pieces.size());
  int left_up_piece_index = 0;
  for (std::unordered_map<MyPoint, PuzzleNode*>::iterator iter = positions.node_from_point.begin(); iter != positions.node_from_point.end(); ++iter)
  {
    MyPoint cur_point = iter->first;
    if (cur_point.get_x() <= min_x && cur_point.get_y() >= max_y)
    {
      min_x = cur_point.get_x();
      max_y = cur_point.get_y();
      left_up_piece_index = iter->second->get_index();
    }
  }

  for (int i = 0; i < puzzle_size + 2 * piece_size; i += piece_size)
  {
    //If result have mistakes we should show pieces that had higher y coordinate and higher x coordinate
    if (i == 0)
      max_y += 1;
    for (int j = puzzle_size, k = 0; j >= 0; j -= piece_size, k++)
    {
      if (positions.contains(MyPoint(min_x + k, max_y)))
      {
        left_up_piece_index = positions.get_node(MyPoint(min_x + k, max_y))->get_index();
        (pieces)[left_up_piece_index].copyTo(image(cv::Rect(j, i, piece_size, piece_size)));
      }
    }
    if (i == 0)
      max_y -= 2;
    else
      max_y -= 1;
  }

  return image;
}

/*
 * Solve puzzle 
 *
 * @param image - puzzle
 * @param piece_size - size of piece in pixel
 */
cv::Mat PuzzleSolver::solve(const cv::Mat &image, const int piece_size)
{
  //Get pieces of puzzle
  std::vector<cv::Mat> pieces = Puzzle::get_pieces(image, piece_size);

  //Create all nodes
  std::vector<PuzzleNode> nodes = std::vector<PuzzleNode>();
  for (int i = 0; i < pieces.size(); i++)
  {
    nodes.push_back(PuzzleNode(i));
  }
  
  //Calc all possible weight  
  for (int i = 0; i < pieces.size(); i++)
    for (int j = i + 1; j < pieces.size(); j++)
      for (const auto pos : all_pos)
        //Choose one of metrics
        cons.push_back(Connection(MGC::calc(pieces[i], pieces[j], pos), pos, i, j));
        //cons.push_back(Connection(SSD::calc(pieces[i], pieces[j], pos), pos, i, j));

  sort(cons.begin(), cons.end());
  while (get_mst_max_size() < pieces.size() && !cons.empty())
  {
    //Pop min connection and try to merge two nodes 
    Connection min_con = cons.back();
    int first_index = min_con.m_first_index;
    int second_index = min_con.m_second_index;
    Position pos = min_con.m_pos;
    cons.pop_back();

    if (check_compatibility(&nodes[first_index], &nodes[second_index], pos))
    {
      merge_nodes(&nodes[first_index], &nodes[second_index], pos);
    }
  }

  cv::Mat result_mat = get_result_image(image.cols, pieces, &nodes);
  nodes.clear();
  pieces.clear();

  cv::imshow("Display window", result_mat);
  cv::waitKey(0);
  cv::imwrite("image.png", result_mat);

  return result_mat;
}


