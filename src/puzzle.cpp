#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "puzzle.h"


/*
 * Get all pieces of given puzzle
 *
 * @param image - puzzle
 * @param piece_size - size of all pieces in puzzle
 * @retutn vector of all pieces
 */
std::vector<cv::Mat> Puzzle::get_pieces(const cv::Mat& image, const int piece_size) {
  std::vector<cv::Mat> pieces = std::vector<cv::Mat>();
  int puzzle_size = image.cols;
  for (int i = 0; i < puzzle_size; i += piece_size)
    for (int j = 0; j < puzzle_size; j += piece_size) {
      pieces.push_back(image(cv::Rect(j, i, piece_size, piece_size)));
  }

  return pieces;
}

/*
* Get side of the given puzzle piece
*
* @param mat - puzzle piece
* @param side - necessary side
* @param near_side - return the border side or adjacent to it (default is false)
* @retutn vector(matrix with Nx1 dim) of side pixels
*/
const cv::Mat Puzzle::get_side(const cv::Mat & mat, SIDE side, bool near_side)
{
  switch (side)
  {
  case UP:
    if (near_side)
      return mat.row(1);
    return mat.row(0);

  case DOWN:
    if (near_side)
      return mat.row(mat.rows - 2);
    return mat.row(mat.rows - 1);

  case LEFT:
    if (near_side)
      return (cv::Mat)mat.col(1).t();
    return (cv::Mat)mat.col(0).t();

  case RIGHT:
    if (near_side)
      return (cv::Mat)mat.col(mat.cols - 2).t();
    return (cv::Mat)mat.col(mat.cols - 1).t();

  default:
    break;
  }

  return cv::Mat();
}
