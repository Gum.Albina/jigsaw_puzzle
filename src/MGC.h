#pragma once
#include <opencv2/core/core_c.h>
#include <opencv2/core.hpp>
#include "puzzle.h"
#include "SSD.h"

/*
 * Class for calculating similarity based on Mahalanobis distance
 */
class MGC {
public:
  static double calc(const cv::Mat & mat1, const cv::Mat & mat2, const Position & pos);
private:
  static double get_dist(const cv::Mat &first_side, const cv::Mat& second_side,
    const cv::Mat& neighbour_for_side);
  static cv::Mat_<double> get_mat_per_channels(const cv::Mat_<cv::Vec3d>& mat);
};