#pragma once
#include "puzzle.h"

/*
 * Class for interpretation puzzle pieces as nodes in graph and their connections as graph edges
 */
class PuzzleNode {
public:
  PuzzleNode(int index);
  ~PuzzleNode() {};
  void set_neighbour(PuzzleNode* neighbour, SIDE side);
  int get_index() { return m_index; }

  PuzzleNode* get_up_neighbour();
  PuzzleNode* get_left_neighbour();
  PuzzleNode* get_down_neighbour();
  PuzzleNode* get_right_neighbour();

  bool operator ==(const PuzzleNode & node) const;
  bool have_neighbour() { return (m_up != nullptr || m_down != nullptr || m_left != nullptr || m_right != nullptr); }

private:
  int m_index;
  PuzzleNode* m_up;
  PuzzleNode* m_left;
  PuzzleNode* m_down;
  PuzzleNode* m_right;
};

/*
 * Hash function for PuzzleNode class 
 */
namespace std
{
  template<>
  struct hash<PuzzleNode>
  {
    size_t operator()(PuzzleNode& node) const
    {
      return hash<int>()(node.get_index());
    }
  };
}