#include "node_coords.h"

/*
 * Add new node to current coordinate plane
 *
 * @param point - coordinates of new node
 * @param node - new node
 */
void Node_coords::insert(MyPoint point, PuzzleNode* node)
{
  point_from_node.insert({ node, point });
  node_from_point.insert({ point, node });
}

/*
* Check if current coordinate plane contains node on given coordinate
*
* @param point - coordinates
* @return true if contains, false otherwise
*/
const bool Node_coords::contains(MyPoint point)
{
  if (node_from_point.find(point) != node_from_point.end())
    return true;

  return false;
}


/*
* Check if current coordinate plane contains given node
*
* @param node - node to check
* @return true if contains, false otherwise
*/
const bool Node_coords::contains(PuzzleNode* node)
{
  if (point_from_node.find(node) != point_from_node.end())
    return true;

  return false;
}