#pragma once
#include <iostream>
#include <cmath>


/*
 * Class for interpretation 2D cootdinate's plane points
 */

class MyPoint {
public:
  MyPoint(int x = INFINITY, int y = INFINITY) : m_x(x), m_y(y) {};
  int get_x() { return m_x; }
  int get_y() { return m_y; }
  bool operator <(const MyPoint & point) const;
  bool operator ==(const MyPoint & point) const;
private:
  int m_x;
  int m_y;
};

/*
 * Hash function for Point class
 */

namespace std
{
  template<>
  struct hash<MyPoint>
  {
    size_t operator()(MyPoint point) const
    {
      return hash<long long>()((long long)point.get_x() << 32 | (long long)point.get_y());
    }
  };
}