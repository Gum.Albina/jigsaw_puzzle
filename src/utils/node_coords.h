#pragma once
#include <unordered_map>
#include "./../puzzle_node.h"
#include "my_point.h"

/*
* Class for bijection between puzzle piece's connections and coordinate plane
*/
class Node_coords {
public:
  MyPoint get_point(PuzzleNode* node) { return point_from_node[node]; };
  PuzzleNode* get_node(MyPoint point) { return node_from_point[point]; };
  const bool empty() { return point_from_node.empty(); };
  void insert(MyPoint point, PuzzleNode* node);
  const bool contains(MyPoint point);
  const bool contains(PuzzleNode* node);
  std::unordered_map<MyPoint, PuzzleNode*>  node_from_point;
private:
  std::unordered_map<PuzzleNode*, MyPoint>  point_from_node;
};