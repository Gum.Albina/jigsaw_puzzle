#include "my_point.h"

/*
 * Overload < operator for Point class
 *
 * Points compare by Euclidean distance to 0 point
 *
 *@return true if first point is nearer to 0, false otherwise
 */
bool MyPoint::operator<(const MyPoint & point) const
{
  return sqrt(m_x * m_x + m_y * m_y) < sqrt(point.m_x * point.m_x + point.m_y * point.m_y);
}

/*
* Overload == operator for Point class
*
* Points are equal if x and y coordinates are equal
*
*@return true if points are equal, false otherwise
*/
bool MyPoint::operator==(const MyPoint & point) const
{
  return (point.m_x == m_x && point.m_y == m_y);
}
