#pragma once
#include "puzzle.h"
#include "SSD.h"
#include "puzzle_node.h"
#include "./utils/node_coords.h"
#include "./utils/my_point.h"


/*
* Container for interpretation pieces connections as graph edges
*/
struct Connection {
  double m_weight;
  Position m_pos;
  int m_first_index;
  int m_second_index;

  Connection(double weight, Position pos, int first_index, int second_index) : m_weight(weight),
    m_pos(pos), m_first_index(first_index), m_second_index(second_index) {};

  bool operator==(const Connection& con) const;
  bool operator<(const Connection& con) const;
};

/*
 * Container for solving puzzle functions
 */
class PuzzleSolver {
public:
  PuzzleSolver();
  ~PuzzleSolver();
  cv::Mat solve(const cv::Mat &image, const int piece_size);
private:
  bool check_compatibility(PuzzleNode* first, PuzzleNode* second, Position pos);
  bool get_neighbours_positions(PuzzleNode* cur_node, Node_coords* positions, int last_node);
  void merge_nodes(PuzzleNode* first, PuzzleNode* second, Position pos);
  int get_mst_max_size() { return m_mst_max_size; }
  void set_mst_max_size(int size) { m_mst_max_size = size; }
  cv::Mat get_result_image(int puzzle_size, const std::vector<cv::Mat>& pieces, std::vector<PuzzleNode>* nodes);

  Puzzle m_puzzle;
  std::vector<Connection> cons;
  int m_mst_max_size;
};


/*
 * Hash function for Connection class instances
 */
namespace std
{
  template<>
  struct hash<Connection>
  {
    inline size_t operator()(Connection con) const
    {
      return hash<long long>()((long long)con.m_pos << 64 | (long long)con.m_first_index << 32 |
        (long long)con.m_second_index);
    }
  };
}